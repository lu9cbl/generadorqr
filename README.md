# generadorQR

Script en python3 que recibe por parámetro un nombre y una URL. Como salida genera una imagen QR (con extensión JPG) para poder ser utilizada en donde se requiera.

Requiere los siguientes paquetes:
```
pip install qrcode
pip install image
```

Se usa de la siguiente manera:
```
python3 generadorqr.py <NombreArchivo> <url>
```
