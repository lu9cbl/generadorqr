#!/usr/bin/env python3

import qrcode
import sys

def qr_gen(filename, data_url):
    
    try:

        img = qrcode.make(data_url)
        img.save(filename)
        print("Se generó correcamente el archivo: " + str(filename))

    except Exception as e:
        print(e)


if __name__ == "__main__":
    
    try:
        #script args
        filename = sys.argv[1]
        data_url = sys.argv[2]
        filename = filename + ".jpg"
        qr_gen(filename, data_url)

    except Exception as e:
        print("*** ¡¡ERROR!! debe usar el siguiente comando: ***")
        print("python3 generadorqr.py <NombreArchivo> <url>")
